beatpicker
=============

A simple audio sample player / auditor

![screenshot](https://bitbucket.org/yphil/beatpicker/raw/master/screenshot.png)

### Dependancies

- python-gst
- python-scipy
- python-matplotlib

You may simply `apt install python-scipy python-matplotlib python-gst0.10` ; Then run `./beatpicker_1.0-1/usr/bin/beatpicker`


Or (simpler, all dependencies will be installed automatically) build the package with

```
sudo chown -R root:root beatpicker_1.0-1 && sudo dpkg-deb --build beatpicker_1.0-1 && sudo chown -R $USER beatpicker_1.0-1*
```

And install it with

```
sudo dpkg -i beatpicker_1.0-1.deb && sudo apt-get install -f
```

Or

```
sudo gdebi beatpicker_1.0-1.deb
```