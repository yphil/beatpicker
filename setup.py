from distutils.core import setup

setup(name = "Beatpicker",
      description = "A small sound player/auditor",
      author = "Yassin Philip",
      author_email = "yassinphil@gmail.com",
      url = "https://bitbucket.org/yassinphilip/beatpicker",
      license='GPLv2',
      scripts=['beatpicker'],
      data_files = [("share/icons/hicolor/16x16/apps", ["icon/16/beatpicker.png"]),
                    ("share/icons/hicolor/22x22/apps", ["icon/22/beatpicker.png"]),
                    ("share/icons/hicolor/24x24/apps", ["icon/24/beatpicker.png"]),
                    ("share/icons/hicolor/32x32/apps", ["icon/32/beatpicker.png"]),
                    ("share/icons/hicolor/48x48/apps", ["icon/48/beatpicker.png"]),
                    ("share/icons/hicolor/64x64/apps", ["icon/64/beatpicker.png"]),
                    ("share/icons/hicolor/72x72/apps", ["icon/72/beatpicker.png"]),
                    ("share/icons/hicolor/96x96/apps", ["icon/96/beatpicker.png"]),
                    ("share/icons/hicolor/128x128/apps", ["icon/128/beatpicker.png"]),
                    ("share/applications", ["beatpicker.desktop"]) ] )
